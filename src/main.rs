extern crate crypto;
extern crate rand;

use std::fs::File;
use std::io::{BufReader,BufRead};
use std::collections::HashMap;
use std::collections::HashSet;

use crypto::md5::Md5;
use crypto::digest::Digest;
use rand::{thread_rng, Rng};


const N: usize = 7;

fn md5head(input: &str) -> String {
    let mut sh = Md5::new();
    sh.input_str(input);
    let mut output = sh.result_str();
    output.truncate(N);
    output
}

fn main() {
    let mut hashes = HashMap::new();
    let file = File::open("partecipanti.txt").unwrap();
    let reader = BufReader::new(&file);
    println!("===========================");
    println!("Partecipanti all'estrazione");
    println!("===========================");
    for line in reader.lines() {
        let input = String::from(line.unwrap().trim());
        if !input.is_empty() {
            let output = md5head(&input);
            println!("MD5({}) prefix = {}", &input, &output);
            hashes.insert(output, input);
        }
    }
    let words_list = File::open("/usr/share/dict/italian").unwrap();
    let words_reader = BufReader::new(&words_list);
    let words_lines = words_reader.lines();
    let words: Vec<_> = words_lines.filter_map(|x| x.ok()).collect();
    let mut seen = HashSet::new();
    println!("=================================================");
    println!("Searching for MD5 prefixes with len={} collisions", &N);
    println!("=================================================");
    let mut words1 = words.to_vec();
    let mut words2 = words.to_vec();
    let mut rng = thread_rng();
    rng.shuffle(&mut words1[..]);
    rng.shuffle(&mut words2[..]);
    for word1 in words1.iter() {
        for word2 in words2.iter() {
            let word = format!("{} {}", word1, word2);
            let test = md5head(&word);
            if hashes.contains_key(&test) & !seen.contains(&test) {
                println!("\nThe winner is '{}'!\n\tFound a collision with '{}'\n", hashes.get(&test).unwrap(), &word);
                seen.insert(test);
                break;
            }
            if seen.len() == hashes.len() {
                println!("All winners were found!");
                return
            }
        }
    }
    println!("{} winners were not found!", hashes.len() - seen.len());
}
